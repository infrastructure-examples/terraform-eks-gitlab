# Setting up a Cluster in EKS
  
Install the [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html)
Install [`jq` the command line JSON parser](https://stedolan.github.io/jq/download/).

Setting up state backend

    cd terraform
    terraform init
    terraform apply -target=null_resource.backend_state_setup -var='gitlab-token=not-needed-yet' -var='gitlab-project-id=14446037' -var='cluster-name=gitlab-eks'  

Edit `backend.hcl`

Setting up the cluster

    terraform init -backend-config=backend.hcl
    terraform apply -var='gitlab-token=your-token' -var='gitlab-project-id=your-project-id' -var='cluster-environment=*' -var='cluster-name=gitlab-eks' -var='aws_region=eu-central-1'

In the above command the following variables are optional, and show their default values:

- cluster-name
- cluster-environment
- aws_region

# Setting your cluster size

Adjust `aws_eks_node_group.node.scaling_config` 

# Helpers

In order to run `kubectl` commands on your cluster, we export the cluster's kubeconfig yaml file to `terraform/.outputs/kubeconfig.yaml`. Moreover, there is a bash function `keks` preset for you. In order to use `keks` run 

    source keks.profile

# Limitations

- [installing Knative](https://www.terraform.io/docs/providers/helm/index.html) should still be done manually
- Knative install can be automated, and [k8s yaml can be converted to tf](https://github.com/sl1pm4t/k2tf)
- once Knative is installed, EKS does not give an IP, but a CNAME record to point to
- the provided IP/CNAME should be set manually

# Resources

This repo is based on https://github.com/terraform-providers/terraform-provider-aws/tree/master/examples/eks-getting-started